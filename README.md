# todo-vuejs

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Image App
<img src="https://gitlab.com/buithanhkhiem/todo-vue/raw/master/Screenshot%20from%202020-01-14%2012-13-48.png">

